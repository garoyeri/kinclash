﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinClash.Sounds
{
    public static class Clips
    {
        public static Uri GameTheme1
        {
            get { return new Uri(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Sounds", "01 Game Theme 1 (driving).mp3")); }
        }

        public static Uri GameTheme2
        {
            get { return new Uri(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Sounds", "02 Game Theme 2 (introspective).mp3")); }
        }

        public static Uri GameTheme3
        {
            get { return new Uri(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Sounds", "03 Game Theme 3 (no melody).mp3")); }
        }

        public static Uri EvilChime
        {
            get { return new Uri(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Sounds", "04 Evil Chime.mp3")); }
        }

        public static Uri RightAnswer
        {
            get { return new Uri(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Sounds", "05 Right Answer.mp3")); }
        }

        public static Uri WrongBuzz
        {
            get { return new Uri(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Sounds", "06 Wrong Answer.mp3")); }
        }

        public static Uri ActionButton
        {
            get { return new Uri(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Sounds", "07 Pushbutton1.mp3")); }
        }

        public static Uri DoubleDing
        {
            get { return new Uri(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Sounds", "08 Pushbutton2-Right Answer2.mp3")); }
        }

        public static Uri Scream
        {
            get { return new Uri(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Sounds", "09 Pushbutton3.mp3")); }
        }

        public static Uri Ding
        {
            get { return new Uri(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Sounds", "10 Pushbutton4.mp3")); }
        }

        public static Uri Trombone
        {
            get { return new Uri(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Sounds", "11 Pushbutton5.mp3")); }
        }
    }
}
