﻿using Caliburn.Micro;
using KinClash.Core;
using System.ComponentModel.Composition;
using System.Dynamic;
using System.Windows;

namespace KinClash.ViewModels
{
    [Export]
    public class MainViewModel : Screen
    {
        private readonly IWindowManager _windowManager;

        [ImportingConstructor]
        public MainViewModel(IWindowManager windowManager)
        {
            DisplayName = "Kin Clash";
            _windowManager = windowManager;

            Scoring = new Core.Scoring();
            Scoring.LeftTeam.Name = "TEAM LEFTO";
            Scoring.RightTeam.Name = "TEAM RIGHTO";

            Scoring.Boards.Add(new Board("Testing Question 12345667?", new[]
            {
                new BoardEntry("Answer 1", 30),
                new BoardEntry("Answer 2", 20),
                new BoardEntry("Answer 3", 10),
                new BoardEntry("Answer 4", 10),
                new BoardEntry("Answer 5", 10),
                new BoardEntry("Answer 6", 10),
                new BoardEntry("Answer 7", 10),
                new BoardEntry(null, 0),
                new BoardEntry(null, 0),
                new BoardEntry(null, 0)
            }));

            Scoring.Reset();
        }

        public Scoring Scoring
        {
            get;
            private set;
        }

        protected override void OnViewLoaded(object view)
        {
            OpenAudienceWindow();
            OpenJudgeWindow();
        }

        public void OpenAudienceWindow()
        {
            _windowManager.ShowWindow(new Audience.MainAudienceViewModel(this.Scoring));
        }

        public void OpenJudgeWindow()
        {
            _windowManager.ShowWindow(new Judge.MainJudgeViewModel(this.Scoring));
        }
    }
}
