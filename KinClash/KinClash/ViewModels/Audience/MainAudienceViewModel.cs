﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using System.ComponentModel.Composition;
using KinClash.Core;

namespace KinClash.ViewModels.Audience
{
    [Export]
    public class MainAudienceViewModel : Screen
    {
        [ImportingConstructor]
        public MainAudienceViewModel(Scoring scoring)
        {
            this.Scoring = scoring;
            this.DisplayName = "Kin Clash!";
        }

        public Scoring Scoring
        {
            get;
            private set;
        }
    }
}
