﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using System.ComponentModel.Composition;
using KinClash.Core;

namespace KinClash.ViewModels.Judge
{
    [Export]
    public class MainJudgeViewModel : Screen
    {
        [ImportingConstructor]
        public MainJudgeViewModel(Scoring scoring)
        {
            Scoring = scoring;
            this.DisplayName = "Judge Window";
            this.Board = new BoardViewModel { Scoring = Scoring };
        }

        public Scoring Scoring
        {
            get;
            private set;
        }

        public BoardViewModel Board
        {
            get;
            private set;
        }
    }
}
