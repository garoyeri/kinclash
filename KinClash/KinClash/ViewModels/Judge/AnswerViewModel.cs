﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using KinClash.Core;

namespace KinClash.ViewModels.Judge
{
    public class AnswerViewModel : PropertyChangedBase
    {
        public BoardEntry Entry { get; set; }
        public Scoring Scoring { get; set; }
        public BoardViewModel Board { get; set; }

        public void Guess()
        {
            if (Entry.IsRevealed) return;

            Scoring.Guess(Entry.Answer);
            Board.PlayEffect(Sounds.Clips.Ding);
        }
    }
}
