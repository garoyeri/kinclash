﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using KinClash.Core;
using System.Windows.Media;

namespace KinClash.ViewModels.Judge
{
    public class BoardViewModel : PropertyChangedBase
    {
        MediaPlayer _themePlayer = new MediaPlayer();
        MediaPlayer _effectPlayer = new MediaPlayer();

        public BoardViewModel()
        {
            _themePlayer.Open(Sounds.Clips.GameTheme1);
        }

        public Scoring Scoring { get; set; }

        public IEnumerable<AnswerViewModel> Answers
        {
            get { return Scoring.CurrentBoard.Answers.Select(a => new AnswerViewModel { Board = this, Entry = a, Scoring = Scoring }); }
        }

        public void GuessWrong()
        {
            PlayEffect(Sounds.Clips.Trombone);
            Scoring.Guess("");
        }

        public void PlayOpening()
        {
            _themePlayer.Play();
        }

        public void StopOpening()
        {
            _themePlayer.Stop();
        }

        public void PlayEffect(Uri effect)
        {
            _effectPlayer.Open(effect);
            _effectPlayer.Play();
        }

        public void SelectLeft()
        {
            Scoring.CurrentTeam = Scoring.LeftTeam;
        }

        public void SelectRight()
        {
            Scoring.CurrentTeam = Scoring.RightTeam;
        }

        public void SelectNone()
        {
            Scoring.CurrentTeam = null;
        }

        public void StartNextGame()
        {
            if (Scoring.State == GameState.Intermission)
            {
                Scoring.NextRound();
            }
        }
    }
}
