﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinClash.Core
{
    public class BoardEntry : PropertyChangedBase
    {
        bool _revealed = false;
        string _answer;
        byte _value;

        public BoardEntry(string answer, byte value)
        {
            this.Answer = answer;
            this.Value = value;
        }

        public string Answer
        {
            get { return _answer; }
            private set
            {
                if (Answer == value) return;
                
                _answer = value;
                NotifyOfPropertyChange();
                NotifyOfPropertyChange(() => IsValid);
            }
        }

        public byte Value
        {
            get { return _value; }
            private set
            {
                if (Value == value) return;

                _value = value;
                NotifyOfPropertyChange();
            }
        }


        public bool IsValid
        {
            get { return !string.IsNullOrEmpty(Answer); }
        }

        public bool IsRevealed
        {
            get { return _revealed; }
            set
            {
                if (value == IsRevealed) return;

                _revealed = value;
                NotifyOfPropertyChange();
            }
        }



        public override bool Equals(object obj)
        {
            var other = obj as BoardEntry;
            if (other == null) return false;

            return this.Answer == other.Answer &&
                this.Value == other.Value &&
                this.IsRevealed == other.IsRevealed;
        }

        public override int GetHashCode()
        {
            return (this.Answer ?? "").GetHashCode();
        }
    }
}
