﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinClash.Core
{
    public class Board
    {
        ObservableCollection<BoardEntry> _entries = new ObservableCollection<BoardEntry>();

        public Board()
        {
        }

        public Board(string title, IEnumerable<BoardEntry> entries)
        {
            this.Title = title;
            foreach (var entry in entries)
                Answers.Add(entry);
        }

        public void Reset()
        {
            foreach (var answer in Answers)
                answer.IsRevealed = false;
        }

        public string Title { get; set; }
        public ObservableCollection<BoardEntry> Answers
        {
            get { return _entries; }
        }

        public BoardEntry FindAnswer(string answer)
        {
            return Answers.FirstOrDefault(a => a.IsValid && (a.Answer == answer));
        }

        public BoardEntry HighestAnswer
        {
            get { return Answers.OrderByDescending(a => a.Value).FirstOrDefault(); }
        }

        public override bool Equals(object obj)
        {
            var other = obj as Board;
            if (other == null) return false;

            return this.Title == other.Title &&
                this.Answers.SequenceEqual(other.Answers);
        }

        public override int GetHashCode()
        {
            return (this.Title ?? "").GetHashCode();
        }
    }
}
