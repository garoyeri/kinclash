﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinClash.Core
{
    public enum GameState
    {
        FirstGuess,
        SecondGuess,
        Playing,
        ChanceToSteal,
        Intermission
    }
}
