﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinClash.Core
{
    public class Team : PropertyChangedBase
    {
        string _name;
        int _score = 0, _misses = 0;
        bool _isCurrent = false;
        private bool _isWinner;

        public string Name
        {
            get { return _name; }
            set
            {
                if (value == this.Name) return;

                _name = value;
                NotifyOfPropertyChange();
            }
        }

        public int Score
        {
            get { return _score; }
            set
            {
                if (value == this.Score) return;

                _score = value;
                NotifyOfPropertyChange();
            }
        }

        public int Misses
        {
            get { return _misses; }
            set
            {
                if (value == this.Misses) return;

                _misses = value;
                NotifyOfPropertyChange();
                NotifyOfPropertyChange(() => MissList);
            }
        }

        public bool IsCurrent
        {
            get { return _isCurrent; }
            set
            {
                if (value == IsCurrent) return;

                _isCurrent = value;
                NotifyOfPropertyChange();
            }
        }

        public bool IsWinner
        {
            get { return _isWinner; }
            set
            {
                if (value == IsWinner) return;

                _isWinner = value;
                NotifyOfPropertyChange();
            }
        }

        public IEnumerable<bool> MissList
        {
            get { return Enumerable.Repeat(true, this.Misses); }
        }

        public override bool Equals(object obj)
        {
            var other = obj as Team;
            if (other == null) return false;

            return this.Name == other.Name &&
                this.Score == other.Score &&
                this.Misses == other.Misses;
        }

        public override int GetHashCode()
        {
            return (this.Name ?? "").GetHashCode();
        }

        public void Reset()
        {
            this.Score = 0;
            this.Misses = 0;
            this.IsWinner = false;
        }
    }
}
