﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KinClash.Core
{
    public class Scoring : PropertyChangedBase
    {
        int _bankScore = 0;
        int _nextBoardIndex = 0;
        Team _currentTeam = null;
        Board _currentBoard = null;
        GameState _gameState = GameState.Intermission;

        public Scoring()
        {
            this.Boards = new List<Board>();
            this.LeftTeam = new Team();
            this.RightTeam = new Team();

            Reset();
        }

        public Team LeftTeam
        {
            get;
            private set;
        }

        public Team RightTeam
        {
            get;
            private set;
        }

        public Team CurrentTeam
        {
            get { return _currentTeam; }
            set
            {
                if (value == CurrentTeam) return;

                if (_currentTeam != null)
                    _currentTeam.IsCurrent = false;

                _currentTeam = value;

                if (_currentTeam != null)
                    _currentTeam.IsCurrent = true;

                NotifyOfPropertyChange();
            }
        }

        public IList<Board> Boards
        {
            get;
            private set;
        }

        public GameState State
        {
            get { return _gameState; }
            private set
            {
                if (value == State) return;

                _gameState = value;
                NotifyOfPropertyChange();
            }
        }

        public int BankScore
        {
            get { return _bankScore; }
            private set
            {
                if (value == this.BankScore) return;

                _bankScore = value;
                NotifyOfPropertyChange();
            }
        }

        public void Reset()
        {
            State = GameState.FirstGuess;
            LeftTeam.Reset();
            RightTeam.Reset();

            this.BankScore = 0;

            foreach (var board in Boards)
            {
                board.Reset();
            }

            _nextBoardIndex = 0;
            CurrentBoard = Boards.FirstOrDefault();
            CurrentTeam = null;
        }

        public void Guess(BoardEntry answer)
        {
            Guess(answer.Answer);
        }

        public void Guess(string answer)
        {
            BoardEntry answerFound = null;

            switch (State)
            {
                case GameState.FirstGuess:
                    // on first guess, we reveal an answer (if right)
                    if (CurrentTeam == null) break;

                    answerFound = CurrentBoard.FindAnswer(answer);
                    if (answerFound == null)
                    {
                        // selected team did not get answer
                        CurrentTeam = GetOtherTeam();
                        State = GameState.SecondGuess;
                    }
                    else
                    {
                        // first guess should be new, just reveal it
                        answerFound.IsRevealed = true;
                        BankScore += answerFound.Value;

                        if (answerFound == CurrentBoard.HighestAnswer)
                        {
                            // team got the highest answer, automatically get to play
                            State = GameState.Playing;
                        }
                        else
                        {
                            // team got a good answer, but not the highest, other team gets a chance
                            CurrentTeam = GetOtherTeam();
                            State = GameState.SecondGuess;
                        }
                    }
                    break;

                case GameState.SecondGuess:
                    // on second guess, we reveal an answer (if right)
                    if (CurrentTeam == null) break;

                    answerFound = CurrentBoard.FindAnswer(answer);
                    if (answerFound == null)
                    {
                        // selected team did not get answer, since other team buzzed first, they get control
                        CurrentTeam = GetOtherTeam();
                        State = GameState.Playing;
                    }
                    else if (answerFound.IsRevealed)
                    {
                        // answer was already revealed, guess again
                    }
                    else
                    {
                        // selected team got the answer
                        if (answerFound.Value > BankScore)
                        {
                            // team got the higher answer, get to play
                            answerFound.IsRevealed = true;
                            BankScore += answerFound.Value;
                            State = GameState.Playing;
                        }
                        else
                        {
                            // team got a lower answer, other team gets to play
                            answerFound.IsRevealed = true;
                            BankScore += answerFound.Value;
                            State = GameState.Playing;
                            CurrentTeam = GetOtherTeam();
                        }
                    }
                    break;

                case GameState.Playing:
                    // normal play
                    if (CurrentTeam == null) break;

                    answerFound = CurrentBoard.FindAnswer(answer);
                    if (answerFound == null)
                    {
                        // selected answer was not found, mark as a miss
                        CurrentTeam.Misses++;

                        // check for steal!
                        if (CurrentTeam.Misses >= 3)
                        {
                            // other team has a chance to steal
                            State = GameState.ChanceToSteal;
                            CurrentTeam = GetOtherTeam();
                        }                        
                    }
                    else if (answerFound.IsRevealed)
                    {
                        // answer was already revealed, guess again
                    }
                    else
                    {
                        // selected answer was found
                        answerFound.IsRevealed = true;
                        BankScore += answerFound.Value;

                        // is this a winning situation?
                        if (!CurrentBoard.Answers.Where(a => a.IsValid).Any(a => !a.IsRevealed))
                        {
                            // all answers were revealed, current team gets the points
                            CurrentTeam.Score += BankScore;
                            CurrentTeam.IsWinner = true;
                            State = GameState.Intermission;
                        }
                    }
                    break;

                case GameState.ChanceToSteal:
                    // one shot at stealing all the points
                    if (CurrentTeam == null) break;

                    answerFound = CurrentBoard.FindAnswer(answer);
                    if (answerFound == null)
                    {
                        // selected answer was not found, other team gets the bank points
                        CurrentTeam.Misses++;
                        CurrentTeam = GetOtherTeam();
                        CurrentTeam.Score += BankScore;
                        CurrentTeam.IsWinner = true;
                        State = GameState.Intermission;
                    }
                    else if (answerFound.IsRevealed)
                    {
                        // answer was already revealed, guess again
                    }
                    else
                    {
                        // selected answer was found, the current team steals
                        BankScore += answerFound.Value;
                        CurrentTeam.Score += BankScore;
                        CurrentTeam.IsWinner = true;
                        State = GameState.Intermission;
                    }
                    break;

                case GameState.Intermission:
                    // reveal guesses, assign no points
                    answerFound = CurrentBoard.FindAnswer(answer);
                    if (answerFound != null)
                    {
                        answerFound.IsRevealed = true;
                    }

                    break;

                default:
                    // other state, ignore guesses
                    break;
            }
        }

        public Team GetOtherTeam(Team team = null)
        {
            if (team == null) team = CurrentTeam;

            if (team == LeftTeam) return RightTeam;

            if (team == RightTeam) return LeftTeam;

            return null;
        }

        public Board CurrentBoard
        {
            get { return _currentBoard; }
            private set
            {
                if (value == this.CurrentBoard) return;

                _currentBoard = value;
                NotifyOfPropertyChange();
            }
        }

        public void NextRound()
        {
            BankScore = 0;
            LeftTeam.Misses = 0;
            RightTeam.Misses = 0;
            CurrentTeam = null;
            CurrentBoard = Boards[++_nextBoardIndex];
            State = GameState.FirstGuess;
        }
    }
}
