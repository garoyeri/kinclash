﻿using NUnit.Framework;
using Should;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KinClash.Core.Tests
{
    [TestFixture]
    public class KinClashScoringTests
    {
        Board board1, board2, board3;

        public KinClashScoringTests()
        {
            board1 = new Board("Title 1",
               new BoardEntry[] {
                    new BoardEntry("Answer A", 50),
                    new BoardEntry("Answer B", 30),
                    new BoardEntry("Answer C", 20)
                });
            board2 = new Board("Title 2",
                new BoardEntry[] {
                    new BoardEntry("Answer D", 40),
                    new BoardEntry("Answer E", 25),
                    new BoardEntry("Answer F", 23),
                    new BoardEntry("Answer G", 12)
                });
            board3 = new Board("Title 3",
                new BoardEntry[] {
                    new BoardEntry("Answer H", 40),
                    new BoardEntry("Answer I", 25),
                    new BoardEntry("Answer J", 23),
                    new BoardEntry("Answer K", 12)
                });
        }

        [Test]
        public void can_create_scoring()
        {
            Assert.DoesNotThrow(() =>
            {
                var scoring = new Scoring();
            });
        }

        [Test]
        public void can_set_team_names()
        {
            var scoring = new Scoring();
            scoring.LeftTeam.Name = "Team A";
            scoring.RightTeam.Name = "Team B";

            scoring.LeftTeam.Name.ShouldEqual("Team A");
            scoring.RightTeam.Name.ShouldEqual("Team B");
        }


        [Test]
        public void can_play_one_round_simple_right_team_wins()
        {
            var scoring = new Scoring();
            scoring.LeftTeam.Name = "Team A";
            scoring.RightTeam.Name = "Team B";
            scoring.Boards.Add(board1);
            scoring.Boards.Add(board2);
            scoring.Reset();

            // rep from each team comes up to buzzers, board is set up, question is asked.
            scoring.CurrentBoard.ShouldEqual(board1);
            scoring.LeftTeam.Score.ShouldEqual(0);
            scoring.RightTeam.Score.ShouldEqual(0);
            scoring.CurrentTeam.ShouldBeNull();
            scoring.BankScore.ShouldEqual(0);

            // right team guesses one (not the top answer)
            scoring.CurrentTeam = scoring.RightTeam;
            scoring.Guess("Answer B");
            scoring.BankScore.ShouldEqual(30);
            scoring.CurrentBoard.FindAnswer("Answer B").IsRevealed.ShouldBeTrue();
            // left team gets a shot, guesses completely wrong, but no points, and no penalties
            scoring.CurrentTeam.ShouldEqual(scoring.LeftTeam);
            scoring.Guess("Answer D");
            scoring.BankScore.ShouldEqual(30);
            scoring.LeftTeam.Misses.ShouldEqual(0);

            // right team chooses to play ( they are still the current team )
            scoring.CurrentTeam.ShouldEqual(scoring.RightTeam);

            // right team guesses one right
            scoring.Guess("Answer C");
            scoring.BankScore.ShouldEqual(50);
            scoring.CurrentBoard.FindAnswer("Answer C").IsRevealed.ShouldBeTrue();
            // missing an answer should add no points, but add a miss
            scoring.Guess("Answer D");
            scoring.BankScore.ShouldEqual(50);
            scoring.RightTeam.Misses.ShouldEqual(1);
            // repeating an answer should not add any more points
            scoring.Guess("Answer C");
            scoring.BankScore.ShouldEqual(50);
            scoring.RightTeam.Misses.ShouldEqual(1);
            scoring.CurrentBoard.FindAnswer("Answer C").IsRevealed.ShouldBeTrue();
            // right team guesses last one right, wins the board
            scoring.Guess("Answer A");
            scoring.BankScore.ShouldEqual(100);
            scoring.RightTeam.Score.ShouldEqual(100);
            scoring.State.ShouldEqual(GameState.Intermission);
        }

        [Test]
        public void can_play_one_round_right_team_almost_wins_left_team_steals()
        {
            var scoring = new Scoring();
            scoring.LeftTeam.Name = "Team A";
            scoring.RightTeam.Name = "Team B";
            scoring.Boards.Add(board1);
            scoring.Boards.Add(board2);
            scoring.Reset();

            // rep from each team comes up to buzzers, board is set up, question is asked.
            scoring.CurrentBoard.ShouldEqual(board1);
            scoring.LeftTeam.Score.ShouldEqual(0);
            scoring.RightTeam.Score.ShouldEqual(0);
            scoring.CurrentTeam.ShouldBeNull();
            scoring.BankScore.ShouldEqual(0);

            // right team guesses one (not the top answer)
            scoring.CurrentTeam = scoring.RightTeam;
            scoring.Guess("Answer B");
            scoring.BankScore.ShouldEqual(30);
            scoring.CurrentBoard.FindAnswer("Answer B").IsRevealed.ShouldBeTrue();
            // left team gets a shot, guesses completely wrong, but no points, and no penalties
            scoring.CurrentTeam.ShouldEqual(scoring.LeftTeam);
            scoring.Guess("Answer D");
            scoring.BankScore.ShouldEqual(30);
            scoring.LeftTeam.Misses.ShouldEqual(0);

            // right team chooses to play ( they are still the current team )
            scoring.CurrentTeam.ShouldEqual(scoring.RightTeam);

            // right team guesses one right
            scoring.Guess("Answer C");
            scoring.BankScore.ShouldEqual(50);
            scoring.CurrentBoard.FindAnswer("Answer C").IsRevealed.ShouldBeTrue();

            // missing an answer should add no points, but add a miss
            scoring.Guess("Answer D");
            scoring.BankScore.ShouldEqual(50);
            scoring.RightTeam.Misses.ShouldEqual(1);

            scoring.Guess("Answer E");
            scoring.BankScore.ShouldEqual(50);
            scoring.RightTeam.Misses.ShouldEqual(2);

            scoring.Guess("Answer F");
            scoring.BankScore.ShouldEqual(50);
            scoring.RightTeam.Misses.ShouldEqual(3);
            scoring.CurrentTeam.ShouldEqual(scoring.LeftTeam);
            scoring.State.ShouldEqual(GameState.ChanceToSteal);

            // left team steals successfully
            scoring.Guess("Answer A");
            scoring.BankScore.ShouldEqual(100);
            scoring.LeftTeam.Score.ShouldEqual(100);
            scoring.RightTeam.Score.ShouldEqual(0);
            scoring.State.ShouldEqual(GameState.Intermission);
        }

        [Test]
        public void can_play_one_round_right_team_strikes_out_left_team_fails_to_steal()
        {
            var scoring = new Scoring();
            scoring.LeftTeam.Name = "Team A";
            scoring.RightTeam.Name = "Team B";
            scoring.Boards.Add(board1);
            scoring.Boards.Add(board2);
            scoring.Reset();

            // rep from each team comes up to buzzers, board is set up, question is asked.
            scoring.CurrentBoard.ShouldEqual(board1);
            scoring.LeftTeam.Score.ShouldEqual(0);
            scoring.RightTeam.Score.ShouldEqual(0);
            scoring.CurrentTeam.ShouldBeNull();
            scoring.BankScore.ShouldEqual(0);

            // right team guesses one (not the top answer)
            scoring.CurrentTeam = scoring.RightTeam;
            scoring.Guess("Answer B");
            scoring.BankScore.ShouldEqual(30);
            scoring.CurrentBoard.FindAnswer("Answer B").IsRevealed.ShouldBeTrue();
            // left team gets a shot, guesses completely wrong, but no points, and no penalties
            scoring.CurrentTeam.ShouldEqual(scoring.LeftTeam);
            scoring.Guess("Answer D");
            scoring.BankScore.ShouldEqual(30);
            scoring.LeftTeam.Misses.ShouldEqual(0);

            // right team chooses to play ( they are still the current team )
            scoring.CurrentTeam.ShouldEqual(scoring.RightTeam);

            // right team guesses one right
            scoring.Guess("Answer C");
            scoring.BankScore.ShouldEqual(50);
            scoring.CurrentBoard.FindAnswer("Answer C").IsRevealed.ShouldBeTrue();

            // missing an answer should add no points, but add a miss
            scoring.Guess("Answer D");
            scoring.BankScore.ShouldEqual(50);
            scoring.RightTeam.Misses.ShouldEqual(1);

            scoring.Guess("Answer E");
            scoring.BankScore.ShouldEqual(50);
            scoring.RightTeam.Misses.ShouldEqual(2);

            scoring.Guess("Answer F");
            scoring.BankScore.ShouldEqual(50);
            scoring.RightTeam.Misses.ShouldEqual(3);
            scoring.CurrentTeam.ShouldEqual(scoring.LeftTeam);
            scoring.State.ShouldEqual(GameState.ChanceToSteal);

            // left team fails to steal
            scoring.Guess("Answer G");
            scoring.BankScore.ShouldEqual(50);
            scoring.RightTeam.Score.ShouldEqual(50);
            scoring.LeftTeam.Score.ShouldEqual(0);
            scoring.CurrentTeam.ShouldEqual(scoring.RightTeam);
            scoring.State.ShouldEqual(GameState.Intermission);
        }

        [Test]
        public void can_play_two_rounds()
        {
            var scoring = new Scoring();
            scoring.LeftTeam.Name = "Team A";
            scoring.RightTeam.Name = "Team B";
            scoring.Boards.Add(board1);
            scoring.Boards.Add(board2);
            scoring.Reset();

            // let left team win round 1
            scoring.CurrentTeam = scoring.LeftTeam;
            scoring.Guess("Answer A");
            scoring.Guess("Answer B");
            scoring.Guess("Answer C");
            scoring.BankScore.ShouldEqual(100);
            scoring.LeftTeam.Score.ShouldEqual(100);
            scoring.State.ShouldEqual(GameState.Intermission);

            scoring.NextRound();
            scoring.CurrentBoard.ShouldEqual(board2);
            scoring.BankScore.ShouldEqual(0);
            scoring.CurrentTeam.ShouldBeNull();
            scoring.LeftTeam.Misses.ShouldEqual(0);
            scoring.RightTeam.Misses.ShouldEqual(0);

            // let right team dominate round 2
            scoring.CurrentTeam = scoring.RightTeam;
            scoring.Guess("Answer D");
            scoring.Guess("Answer E");
            scoring.Guess("Answer F");
            scoring.Guess("Answer G");
            scoring.BankScore.ShouldEqual(100);
            scoring.RightTeam.Score.ShouldEqual(100);
            scoring.State.ShouldEqual(GameState.Intermission);
        }

        [Test]
        public void can_play_three_rounds()
        {
            var scoring = new Scoring();
            scoring.LeftTeam.Name = "Team A";
            scoring.RightTeam.Name = "Team B";
            scoring.Boards.Add(board1);
            scoring.Boards.Add(board2);
            scoring.Boards.Add(board3);
            scoring.Reset();

            // let left team win round 1
            scoring.CurrentTeam = scoring.LeftTeam;
            scoring.Guess("Answer A");
            scoring.Guess("Answer B");
            scoring.Guess("Answer C");
            scoring.BankScore.ShouldEqual(100);
            scoring.LeftTeam.Score.ShouldEqual(100);
            scoring.State.ShouldEqual(GameState.Intermission);

            // start round 2
            scoring.NextRound();
            scoring.CurrentBoard.ShouldEqual(board2);
            scoring.BankScore.ShouldEqual(0);
            scoring.CurrentTeam.ShouldBeNull();
            scoring.LeftTeam.Misses.ShouldEqual(0);
            scoring.RightTeam.Misses.ShouldEqual(0);

            // let right team dominate round 2
            scoring.CurrentTeam = scoring.RightTeam;
            scoring.Guess("Answer D");
            scoring.Guess("Answer E");
            scoring.Guess("Answer F");
            scoring.Guess("Answer G");
            scoring.BankScore.ShouldEqual(100);
            scoring.RightTeam.Score.ShouldEqual(100);
            scoring.State.ShouldEqual(GameState.Intermission);

            // start round 3
            scoring.NextRound();
            scoring.CurrentBoard.ShouldEqual(board3);
            scoring.BankScore.ShouldEqual(0);
            scoring.CurrentTeam.ShouldBeNull();
            scoring.LeftTeam.Misses.ShouldEqual(0);
            scoring.RightTeam.Misses.ShouldEqual(0);

            // let left team win round 3 barely
            scoring.CurrentTeam = scoring.RightTeam;
            scoring.Guess("Answer J");
            scoring.Guess("Answer I");
            scoring.CurrentTeam.ShouldEqual(scoring.LeftTeam);
            scoring.BankScore.ShouldEqual(48);
            scoring.Guess("Answer Z");
            scoring.Guess("Answer Y");
            scoring.Guess("Answer X");
            scoring.CurrentTeam.ShouldEqual(scoring.RightTeam);
            scoring.Guess("Answer W");
            scoring.CurrentTeam.ShouldEqual(scoring.LeftTeam);
            scoring.BankScore.ShouldEqual(48);
            scoring.LeftTeam.Score.ShouldEqual(148);
            scoring.State.ShouldEqual(GameState.Intermission);
        }
    }
}
